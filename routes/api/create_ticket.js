var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');

router.post('/create_ticket',function(req,res){
    if(req.body
      && req.body.author
      && req.body.username
      && req.body.name
      && req.body.priority
      && req.body.description
      && req.body.type) {
      if(req.session.uuid == req.body.author) {
        utils.createTicket(req.body, function(callback) {
          if(callback==1) {
            res.send({"response" : 1, "message" : "Oh crap, something went wrong while creating the ticket!"})
          } else {
            res.send({"response" : 0})
          }
        });
      }
      res.send({"response" : 0})
    } else {
      res.send({"response" : 1, "message" : "Please fill in all fields."})
    }
});

module.exports = router;
