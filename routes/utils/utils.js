var express = require('express');
var app = express.Router();
var db = require("../database/connect");
var MongoClient = require('mongodb').MongoClient;
var bcrypt = require('bcrypt-nodejs');
var crypto = require('crypto');
var assert = require('assert');
var mongoose = require('mongoose');
var ObjectID = require('mongodb').ObjectID;
var validate_ID = mongoose.Types.ObjectId;
var User = require('../models/user');
var Year = require('../models/year');
var Student = require('../models/student');
var Code = require('../models/code');
var Class = require('../models/class');
var Group = require('../models/group');
var Report = require('../models/report');
var Ticket = require('../models/ticket');
var Appointment = require('../models/appointment');
var jwt = require('jsonwebtoken');
/*var data = {
  _id: response._id,
  name : result[0],
  email : result[1],
  bio : result[2],
  group : result[3],
  password_changed: false
}*/

studentEditAccount = function(obj, callback) {
  if(validate_ID.isValid(obj._id)) {
    User.update({_id: new ObjectID(obj._id)}, {$set : {
      "students" : obj.students.split(",")}}, function(err, doc) {
      if(doc && !err) {
        callback(0);
      } else {
        callback(1);
      }
    });
  } else {
    callback(1);
  }
}

editAccount = function(obj, callback) {
  if(validate_ID.isValid(obj._id)) {
    if(obj.password_changed == false) {
      User.update({_id: new ObjectID(obj._id)}, {$set : {
        "name" : obj.name,
        "email" : obj.email,
        "bio" : obj.bio,
        "group" : obj.group}}, function(err, doc) {
        if(doc && !err) {
          callback(0);
        } else {
          callback(1);
        }
      });
    } else {
      bcrypt.hash(obj.password, null, null, function(err, hash) {
        if(err) {
          callback(0)
        } else {
          User.update({_id: new ObjectID(obj._id)}, {$set : {
            "name" : obj.name,
            "email" : obj.email,
            "bio" : obj.bio,
            "group" : obj.group,
            "password" : password}}, function(err, doc) {
            if(doc && !err) {
              callback(0);
            } else {
              callback(1);
            }
          });
        }
      });
    }
  } else {
    callback(1)
  }
}

editAppointment = function(obj, callback) {
  if(validate_ID.isValid(obj._id)) {
    Appointment.update({_id: new ObjectID(obj._id)}, {$set : {"time" : obj.time, "date" : obj.date, "type" : obj.type, "status" : obj.status, "additional_details" : obj.additional_details}}, function(err, doc) {
      if(doc && !err) {
        callback(0);
      } else {
        callback(1);
      }
    });
  } else {
    callback(1)
  }
}

updateAccount = function(uuid, name, bio, callback) {

  if(validate_ID.isValid(uuid)) {
    User.update({_id: new ObjectID(uuid)}, {$set : {"bio" : bio, "name" : name}}, function(err, doc) {
      if(doc && !err) {
        console.log('d0c');
        callback(0);
      } else {
        console.log('err');
        callback(1);
      }
    });
  } else {
    console.log('invalid uuid');
    callback(1);
  }
}

getTickets = function(uuid, show_closed, callback) {
  if(validate_ID.isValid(uuid)) {
    var query = {author: uuid, status: "Open"};
    if(show_closed == 'true') {
      query = {author: uuid}
    }
    Ticket.find(query, function(err, items) {
      if(items.length) {
        callback(items);
      } else {
        callback(1);
      }
    });
  } else {
    callback(1);
  }
}

getAllStudents = function(callback) {
  Student.find({}, function(err, items) {
    if(items.length) {
      callback(items);
    } else {
      callback(1);
    }
  });
}

getAllGroups = function(callback) {
  Group.find({}, function(err, items) {
    if(items.length) {
      callback(items);
    } else {
      callback(1);
    }
  });
}

getAllClasses = function(callback) {
  Class.find({}, function(err, items) {
    if(items.length) {
      callback(items);
    } else {
      callback(1);
    }
  });
}

getAllReports = function(callback) {
  Report.find({}, function(err, items) {
    if(items.length) {
      callback(items);
    } else {
      callback(1);
    }
  });
}

getAllTickets = function(callback) {
  Ticker.find({}, function(err, items) {
    if(items.length) {
      callback(items);
    } else {
      callback(1);
    }
  });
}

getAllCodes = function(callback) {
  Code.find({}, function(err, items) {
    if(items.length) {
      callback(items);
    } else {
      callback(1);
    }
  });
}

getAllAppointments = function(callback) {
  Appointment.find({}, function(err, items) {
    if(items.length) {
      callback(items);
    } else {
      callback(1);
    }
  });
}

getAllAccounts = function(callback) {
  User.find({}, function(err, items) {
    if(items.length) {
      callback(items);
    } else {
      callback(1);
    }
  });
}

getAllYears = function(callback) {
  Year.find({}, function(err, items) {
    if(items.length) {
      callback(items);
    } else {
      callback(1);
    }
  });
}

getClassByUUID = function(uuid, callback) {
  if(validate_ID.isValid(uuid)) {
    Class.find({_id: new ObjectID(uuid)}, function(err, items) {
      if(items.length) {
        callback(items[0]);
      } else {
        callback(1);
      }
    });
  } else {
    callback(1);
  }
}


// FIX THIS -> callbacks 1
getReportData = function(uuid, callback) {
  if(validate_ID.isValid(uuid)) {
    Report.find({_id: new ObjectID(uuid)}, function(err, items) {
      if(items.length) {
        console.log("nom");
        callback(items[0]);
      } else {
        console.log("err");
        callback(1);
      }
    });
  } else {
    console.log("unvalidated");
    callback(1);
  }
}

getStudentReports = function(uuid, callback) {
  Report.find({student: uuid}, function(err, items) {
    if(items.length) {
      callback(items);
    } else {
      callback(1);
    }
  });
}

getStudentData = function(uuid, callback) {
  if(validate_ID.isValid(uuid)) {
    Student.find({_id: new ObjectID(uuid)}, function(err, items) {
      if(items.length) {
        callback(items[0]);
      } else {
        callback(1);
      }
    });
  } else {
    callback(1);
  }
}

isLoggedIn = function(req, next) {
  if (req.session.uuid == undefined || req.session.uuid == null) {
    return next(false);
  } else {
    return next(true);
  }
}

groupSelect = function(group, callback) {
  Group.findOne({group_id: group},function(err, item) {
    if(item) {
      callback(item.group_name);
    } else {
      callback(1);
    }
  });
}

getChildrenByUUID = function(uuid, callback) {
    if(validate_ID.isValid(uuid)) {
      User.findOne({_id: new ObjectID(uuid)}, function(err, items) {
        if(items) {
          obj = items;
          students = obj.students;
          console.log(students);
          if(students.length == 0) {
            callback(1)
          } else {
            var studentsArray = [];
            var arrayLength = students.length;
            processed = 0;
            for (var i = 0; i < arrayLength; i++) {
              if(validate_ID.isValid(students[i])) {
                Student.find({_id: new ObjectID(students[i])}, function(err, items) {
                  if(items[0]) {
                    studentsArray.push(items[0]);
                  }
                  processed = processed + 1;
                  if(processed == arrayLength) {
                    callback(studentsArray);
                  }
                });
              } else {
                callback(1);
              }
            }
          }
        } else {
          callback(1);
        }
      });
    } else {
      callback(1);
    }
}

getYearData = function(uuid, callback) {
    if(validate_ID.isValid(uuid)) {
      Year.find({"_id": new ObjectID(uuid)}, function(err, items) {
        if(items.length) {
          callback(items[0]);
        } else {
          callback(1);
        }
      });
    } else {
      callback(1);
    }
}

getTicketData = function(uuid, callback) {
    if(validate_ID.isValid(uuid)) {
      Ticket.find({"_id": new ObjectID(uuid)}, function(err, items) {
        if(items.length) {
          callback(items[0]);
        } else {
          callback(1);
        }
      });
    } else {
      callback(1);
    }
}

getAppointmentData = function(uuid, callback) {
    if(validate_ID.isValid(uuid)) {
      Appointment.find({"_id": new ObjectID(uuid)}, function(err, items) {
        if(items.length) {
          callback(items[0]);
        } else {
          callback(1);
        }
      });
    } else {
      callback(1);
    }
}

getUserDataByEmail = function(email, callback) {
    User.find({email: email}, function(err, items) {
      if(items.length) {
        callback(items[0]);
      } else {
        callback(1);
      }
    });
}


getUserData = function(uuid, callback) {
  if(validate_ID.isValid(uuid)) {
    User.find({_id: new ObjectID(uuid)}, function(err, items) {
      if(items.length) {
        callback(items[0]);
      } else {
        callback(1);
      }
    });
  } else { callback(1); }
}

loginCheck = function(email, password, callback) {
  User.findOne({email: email}, function(err, user) {
    if(user) {
      getGroupByUUID(user._id, function(group) {
        if(group == 1) {
          callback({success: false, msg: 'Authentication failed. Invalid email'});
        } else {
          console.log(password);
          bcrypt.compare(password, user.password, function (err, isMatch) {
            if (isMatch === true && !err) {
              var keys = require('../database/keys');
              token = keys.SECRET_PARENT_KEY;
              url = '/main/dashboard/false';
              if(group.admin == true) {
                token = keys.SECRET_ADMIN_KEY;
                url = '/admin/dashboard';
              }
              var token = jwt.sign(user._id.toString(), token.toString());

              callback({
                success: true,
                message: 'Enjoy your token!',
                url: url,
                token: token,
                _id: user._id
              });
            } else {
              callback({success: false, msg: 'Authentication failed. Wrong password.'});
            }
          });
        }
      });
    } else {
      callback({success: false, msg: 'Authentication failed. Wrong email.'});
    }
  });
}

getTeacherData = function(uuid, callback) {
  if(validate_ID.isValid(uuid)) {
    User.find({_id: new ObjectID(uuid)}, function(err, items) {
      if(items.length) {
        callback(items[0]);
      } else {
        callback({"response" : 1});
      }
    });
  } else { callback(1); }
}

getAppointmentsByUUIDwithHidden = function(uuid, callback) {
  Appointment.find({"parent_id" : uuid},function(err, items) {
    if(items.length) {
      callback(items);
    } else {
      callback({"response" : 1});
    }
  });
}

checkAppointmentAvaliability = function(obj, callback) {
  Appointment.find({"date" : obj.date, "time" : obj.time, "teacher_id" : obj.teacher_id, "status": { $gt: -1, $lt: 4}},function(err, items) {
    if(items.length) {
      callback({"response" : 1});
    } else {
      callback({"response" : 0});
    }
  });
}

getAppointmentsByUUIDwoHidden = function(uuid, callback) {
  Appointment.find({"parent_id" : uuid, "status": { $gt: -1, $lt: 4}},function(err, items) {
    if(items.length) {
      callback(items);
    } else {
      callback({"response" : 1});
    }
  });
}

getTeachers = function(callback) {
  User.find({group: 1},function(err, items) {
    if(items.length) {
      callback(items);
    } else {
      callback({"response" : 1});
    }
  });
}

deleteTicket = function(uuid, callback) {
    if(validate_ID.isValid(uuid)) {
      Ticket.findOneAndUpdate({_id: new ObjectID(uuid)}, {"status" : "Closed"}, function(err, obj) {
        if(!err) {
          callback(0);
        } else {
          callback(1);
        }
      });
    } else { callback(1); }
}

deleteStudentAdmin = function(uuid, callback) {
    if(validate_ID.isValid(uuid)) {
      Student.remove({_id: new ObjectID(uuid)}, function(err, obj) {
        if(!err) {
          callback(0);
        } else {
          callback(1);
        }
      });
    } else { callback(1); }
}

deleteAccountAdmin = function(uuid, callback) {
    if(validate_ID.isValid(uuid)) {
      User.remove({_id: new ObjectID(uuid)}, function(err, obj) {
        if(!err) {
          callback(0);
        } else {
          callback(1);
        }
      });
    } else { callback(1); }
}

deleteAppointmentAdmin = function(uuid, callback) {
    if(validate_ID.isValid(uuid)) {
      Appointment.remove({_id: new ObjectID(uuid)}, function(err, obj) {
        if(!err) {
          callback(0);
        } else {
          callback(1);
        }
      });
    } else { callback(1); }
}

deleteAppointment = function(uuid, callback) {
    if(validate_ID.isValid(uuid)) {
      Appointment.findOneAndUpdate({_id: new ObjectID(uuid)}, {"status" : 4}, function(err, obj) {
        if(!err) {
          callback(0);
        } else {
          callback(1);
        }
      });
    } else { callback(1); }
}

getGroupByEmail = function(email, callback) {

  getUserDataByEmail(email, function(data) {
    if(data == 1) {
      callback(data);
    } else {
      Group.find({group_id: data.group}, function(err, items) {
        if(items.length) {
          callback(items[0]);
        } else {
          callback(1);
        }
      });
    }
});
}

getGroupByUUID = function(id, callback) {
      getUserData(id, function(data) {
        if(data == 1) {
          callback(1);
        } else {
          var group = data.group;
          Group.find({group_id: group},function(err, items) {
            if(items.length) {
              callback(items[0]);
            } else {
              callback(1);
            }
          });
        }
    });
}

getGroupById = function(id, callback) {
  Group.find({group_id: id},function(err, items) {
    if(items.length) {
      callback(items[0]);
    } else {
      callback(1);
    }
  });
}

checkEmailTaken = function(email, callback) {

  User.find({email: email},function(err, items) {
    if(items.length) {
      callback(true);
    } else {
      callback(false);
    }
  });
}

getAuthTokenData = function(auth_token, callback) {
  Code.find({code: auth_token},function(err, items) {
    if(items.length) {
      callback(items[0]);
    } else {
      callback(false);
    }
  });
}

generateEmailVerificationToken = function(callback) {
  var current_date = (new Date()).valueOf().toString();
  var random = Math.random().toString();
  var crypt = crypto.createHash('sha1').update(current_date + random).digest('hex');
  callback(crypt);
}

editStudent = function(data, callback) {
  if(validate_ID.isValid(data._id)) {
    Student.update({_id: new ObjectID(data._id)}, {$set : {
      "name" : data.name,
      "classes" : data.classes,
      "picture_url" : data.picture_url,
      "dob" : data.dob
    }}, function(err, doc) {
      if(doc && !err) {
        callback(0);
      } else {
        callback(1);
      }
    });
  } else {
    console.log('4')
    callback(1);
  }
}

createStudent = function(data, callback) {
    var student = Student({
      "name" : data.name,
      "classes" : data.classes,
      "picture_url" : data.picture_url,
      "dob" : data.dob
    }).save(function(err) {
      if (err) {
        callback(1);
      } else {
        callback(0);
      }
    });
}

createAppointment = function(parent, student, teacher, time, date, additional_details, type, callback) {
    var appointment = Appointment({
      "parent_id" : parent,
      "student_id" : student,
      "teacher_id" : teacher,
      "time" : time,
      "date" : date,
      "status" : 0,
      "additional_details" : additional_details,
      "type" : type
    }).save(function(err) {
      if (err) {
        callback(1);
      } else {
        callback(0);
      }
    });
}

createTicket = function(data, callback) {
  obj = {
    "name" : data.name,
    "description" : data.description,
    "type" : data.type,
    "priority" : data.priority,
    "author" : data.author,
    "messages" : [
      {
        id: 1,
        date: new Date(),
        username: data.username,
        message: data.description
      }
    ]
  }
  Ticket(obj).save(function(err) {
    if (err) {
      callback(1);
    } else {
      callback(0);
    }
  });
}

module.exports = {
  isLoggedIn: isLoggedIn,
  loginCheck: loginCheck,
  getAuthTokenData: getAuthTokenData,
  checkEmailTaken: checkEmailTaken,
  //createAccount: createAccount,
  getUserData: getUserData,
  getStudentData: getStudentData,
  getYearData: getYearData,
  groupSelect: groupSelect,
  getChildrenByUUID: getChildrenByUUID,
  getTeachers: getTeachers,
  getAppointmentsByUUIDwoHidden: getAppointmentsByUUIDwoHidden,
  getAppointmentsByUUIDwithHidden: getAppointmentsByUUIDwithHidden,
  createAppointment: createAppointment,
  getTeacherData: getTeacherData,
  getGroupByGroupId: getGroupById,
  getGroupByUUID: getGroupByUUID,
  getGroupByEmail: getGroupByEmail,
  deleteAppointment: deleteAppointment,
  deleteAppointmentAdmin: deleteAppointmentAdmin,
  getAppointmentData: getAppointmentData,
  checkAppointmentAvaliability: checkAppointmentAvaliability,
  getStudentReports: getStudentReports,
  getClassByUUID: getClassByUUID,
  updateAccount: updateAccount,
  getReportData: getReportData,
  createTicket: createTicket,
  getTickets: getTickets,
  getTicketData: getTicketData,
  deleteTicket: deleteTicket,
  getAllYears:getAllYears,
  getAllCodes: getAllCodes,
  getAllAppointments: getAllAppointments,
  getAllTickets: getAllTickets,
  getAllCodes: getAllCodes,
  getAllClasses: getAllClasses,
  getAllReports: getAllReports,
  getAllAccounts: getAllAccounts,
  getAllGroups: getAllGroups,
  editAppointment: editAppointment,
  deleteAccountAdmin: deleteAccountAdmin,
  editAccount: editAccount,
  getAllStudents: getAllStudents,
  deleteStudentAdmin: deleteStudentAdmin,
  createStudent: createStudent,
  studentEditAccount: studentEditAccount,
  editStudent: editStudent
}
