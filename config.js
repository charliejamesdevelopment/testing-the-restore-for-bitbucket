module.exports = {
  website: {
    url: "http://localhost:3003"
    // Simply insert the base url of your website
    // DO NOT PUT "/" on end of STRING!
    // Changing this value will affect the whole database/application structure
  },
  years: {
    start_school_at_age: 5, // DO NOT put any of these numbers in quotes or ''
    year_start_month: 9,
    year_start_day: 2 // Date that the year starts (excluding inset days, etc)
  },
  appointment: {
    times: ["12:00pm", "13:00pm", "14:00pm", "15:00pm", "16:00pm", "17:00pm"], // Times a parent can book an appointment
    types: [ // Types of appointment meetings
      {
        id: 0,
        name: "Physical Meeting"
      },
      {
        id: 1,
        name: "Phone Call"
      }
    ]
  }
}
